# CNC Wire Plotting

This page documents work developing a computer controlled plotting process for fine wires.  In short, wire is dispensed from a spool and is pressed onto an adhesive substrate by a four axis motion system, effectively drawing with wire.  This workflow could be used to create sensors, actuators, motors, speakers, heaters, manipulators, board games, levitation stages, ...   

<img src='img/sprung-plotter.jpg' height=400px>

With proper wire handling and tension, this process can be quite fast, operating at the limits of the motion system.  The video below is plotting at 1000mm/s.

<img src='img/lvdtape-plotting.mp4' height=300px>

<img src='img/three-phase-plotting-2-720.mp4' height=300px>

Magnet wire is inexpensively available in a variety of gauges, and the integral insulation allows wires to be packed more densely than possible in flex circuit board manufacturing.  For instance, in the planar lorentz force actuators below, the 80 micron copper wire (40AWG) has 10 micron insulation thickness, and so is packed with 20 micron spacing (single insulation build could give 10 micron separation at 40AWG and 4 micron at 45AWG).  The coils can be laminated to a variety of substrates including polymer films like Kapton and PET, as well as composite prepregs like Carbon, Fiberglass, etc.

<img src='img/flatcoil-40awg-microscope.jpg' height=300px>
<img src='img/flatcoil-cut.jpg' height=300px>

Three phase actuators in multiple configurations:

<img src='img/spreadcoils.jpg' height=220px>
<img src='img/three-phase.jpg' height=220px>
<img src='img/three-phase-2.jpg' height=220px>

Planar magnetic speakers (<a href='https://gitlab.cba.mit.edu/calischs/speaker-plotting'>my session for How to Make wildcard week</a>):

<img src='img/speaker-microscope.jpg' height=300px>
<img src='img/hex-spiral-coil.jpg' height=300px>

plotting: 

<img src='img/speaker-plotting.mp4' width=300px>

driving: 

<img src='img/mode-150hz-1000fps.mp4' width=300px>

The head is designed to press the wire down with as little friction as possible, while constraining the wire position as close to the application point as possible without interfering with already-placed wires.  It mounts kinematically to the tool head suspension via a dowel pin and four ball-ended 100 TPI micro-adjustment screws, allowing the wire application point to be precisely aligned with the tool's rotational axis.

<img src='img/plothead-edm-2.jpg' height=300px>
<img src='img/plothead-edm.jpg' height=300px>

Plotting is done flat, but the substrate can be folded into three dimensional configurations.  More on this soon.

Also, other wires can be plotted similarly.  More on this soon.

